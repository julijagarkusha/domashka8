const createCalculator = (base) => {
    const validator = (data) => !isNaN(data);

    const operationValidator = (operand, result) => {
        if (!validator(operand)) {
            return number;
        }

        number = result;
        return number;
    }

    if (!validator(base)) {
        return NaN;
    }

    let number = base;

    return {
        add: (operand) => {
            operationValidator(operand, number + operand);
        },
        sub: (operand) => {
            operationValidator(operand, number - operand);
        },
        set: (newBase) => {
            if (!validator(newBase)) {
                return number;
            }

            number = newBase;
        },
        get: () => number,
        reset: () => {
            number = base;
        }
    }
}

const calculator = createCalculator(100);

calculator.add(10); // 110 - это текущее значение base
calculator.add(10);
calculator.sub(20);

calculator.set(20);
calculator.add(10);
calculator.add(10);
calculator.add('qwe'); // NaN и значение 40 не менять

console.log(calculator.get()) // 40

calculator.reset();
console.log(calculator.get()) // 100
